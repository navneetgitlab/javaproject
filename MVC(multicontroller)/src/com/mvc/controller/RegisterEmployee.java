package com.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegisterEmployee
{
	@RequestMapping("/register")
	public ModelAndView registerEmployee(HttpServletRequest req,HttpServletResponse res)
	{
		String ennumber=req.getParameter("eno");
		String ename=req.getParameter("ename");
		String esalary=req.getParameter("esal");
		String eaddress=req.getParameter("eaddress");
		String print=" "+ennumber+"<br>"+ename+"<br>"+esalary+"<br>"+eaddress+"<br>";
		
		return new ModelAndView("details","print",print);
		
	}

}
