package com.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController
{
	@RequestMapping("/hello")
	public ModelAndView helloworld()
	{
		String message="hello mvc how aare you.......";
		return new ModelAndView("hellopage","message",message);
	}

}
