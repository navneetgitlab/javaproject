package com.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TextController 
{
	@RequestMapping("/hellotext")
	public ModelAndView textDemo(HttpServletRequest req,HttpServletResponse res)
	{
		String str=req.getParameter("t1");
		String message="Hello"+str;
		return new ModelAndView("textpage","message",message);
	}

}
